package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component

class BasketBallCoach implements Coach{

    @Autowired
    @Qualifier(value = "fileFortuneService")
    private FortuneService fortune;


    @PostConstruct
    public void doMyStartupStuff() {

        System.out.println(" >> inside of my doMyStartupStuff");
    }
    @PreDestroy
    public void doMyCleanupStaff() {

        System.out.println(" >> inside of my doMyStartupStuff");
    }

    @Override
    public String getDailyWorkout() {
        return "today you have to pratice shot to basket";
    }

    @Override
    public String getDailyFortune() {
        return fortune.getFortune();
    }


}
