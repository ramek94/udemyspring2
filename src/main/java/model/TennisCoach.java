package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("mojTrener")
public class TennisCoach implements Coach {



	@Autowired
	@Qualifier(value = "fileFortuneService")
	private FortuneService fortune;



	@Override
	public String getDailyWorkout() {
		
		return "today you have to pratice";
	}

	@Override
	public String getDailyFortune() {
		return fortune.getFortune();
	}


}
