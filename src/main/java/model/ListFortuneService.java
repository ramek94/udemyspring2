package model;


import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class ListFortuneService implements FortuneService {

    private String[] fortunes = {"1 motywator","2 motywator","3 motywator"};
    private Random myRandom = new Random();
	@Override
	public String getFortune() {
		
		   int index = myRandom.nextInt(fortunes.length);
	        String theFortune=fortunes[index];
	        return theFortune;
	}

    
    
}
