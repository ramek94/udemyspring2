package model;

import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class FileFortuneService implements FortuneService {

    File file;
    Random random;
    String fortuna;
    List<String> listaFortunes = new ArrayList<String>();

    public FileFortuneService() throws IOException {

        file = new File("C:\\projekty-JSA\\UdemyNaukaSpring2\\src\\main\\resources\\fileFortunes.txt");
        random = new Random();


        System.out.println("Reading fortunes from file: " + file);
        System.out.println("File exists: " + file.exists());

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String tmpLine;


            while ((tmpLine = br.readLine()) != null) {
                listaFortunes.add(tmpLine);
            }




        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }
    }


    @Override
    public String getFortune() {
        return listaFortunes.get(random.nextInt(listaFortunes.size())) ;
    }
}
