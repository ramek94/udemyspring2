package model;

public interface Coach {

	public String getDailyWorkout();

	public String getDailyFortune();
	  
}
