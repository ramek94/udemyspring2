package demo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.Coach;

public class AnnotationDemoApp {


	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		Coach coach = context.getBean("mojTrener",Coach.class);

		Coach coachBasket= context.getBean("basketBallCoach", Coach.class );
		System.out.println(coach.getDailyFortune());
		System.out.println(coachBasket.getDailyFortune());
		context.close();



	}
	

	
	
}
