package demo;

import model.Coach;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnotationScopeDemoApp {


    public static void main(String[] args) {

        // load spring config

        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        // retrive bean from spring container


        Coach pierwszyCoach = context.getBean("basketBallCoach", Coach.class);
        Coach drugiCoach = context.getBean("basketBallCoach", Coach.class);

        boolean  result = (pierwszyCoach==drugiCoach);

        System.out.println("\nPointing to the same object" + result);
        System.out.println("Place in memory" + pierwszyCoach);
        System.out.println("Place in memory" + drugiCoach);

        context.close();
    }
}
